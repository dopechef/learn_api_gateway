const cluster = require('cluster');
const numWorkers = require('os').cpus().length;
const express = require('express');
const bodyParser = require('body-parser');
const mws = require('./config/middlewares');
const routes = require('./app/routes/index');
const { port, env } = require('./config/config');
const { validateUserToken } = require('./app/middleware/auth');

const app = express();
require('dotenv').config();
// use middlewares
app.use(mws);
// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/v1/auth', routes.auth);

// validate all routes from here
app.use(validateUserToken);


// user
app.use('/v1/user/announcement', routes.announcement);
app.use('/v1/user/course', routes.course);
app.use('/v1/user/lesson', routes.lesson);
app.use('/v1/user/path', routes.path);
app.use('/v1/user', routes.user);
app.use('/v1/user/interests', routes.interest);
app.use('/v1/user/organization', routes.organization);
app.use('/v1/user/group', routes.group);

// admin
app.use('/v1/admin/organization', routes.admin_org);
app.use('/v1/admin/group', routes.admin_grp);
app.use('/v1/admin/course', routes.admin_crs);
app.use('/v1/admin/lesson', routes.admin_lsn);
app.use('/v1/admin/path', routes.admin_pth);
app.use('/v1/admin/annoucement', routes.admin_aun);
app.use('/v1/admin/interest', routes.admin_int);
app.use('/v1/admin/user', routes.admin_user);


console.log('hello');
// connect to server custer
if (cluster.isMaster) {
  console.log(`Master cluster setting up ${numWorkers} workers...`);

  for (let i = 0; i < numWorkers; i++) {
    cluster.fork();
  }

  cluster.on('online', (worker) => {
    console.log(`Worker ${worker.process.pid} is online`);
  });

  cluster.on('exit', (worker, code, signal) => {
    console.log(`Worker ${worker.process.pid} died with code: ${code}, and signal: ${signal}`);
    console.log('Starting a new worker');
    cluster.fork();
  });
} else {
  app.listen(port, () => console.info(`server started on port ${port} (${env})`));
}
