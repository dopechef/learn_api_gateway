const express = require('express');

const app = express();

const api = express.Router();
const users = require('../../.././rpcClients/userCli');
const { validateUserToken } = require('../../../middleware/auth');

// enable authentication
app.use(validateUserToken);

api.route('/')
  .get(users.GetUsers);

api.get('/users/deleted', users.GetDeletedUsers);

api.route('/:id')
  .put(users.UpdateUser)
  .get(users.GetUser)
  .delete(users.DeleteUser);
api.put('/user/restore/:id', users.RestoreUser);

api.put('/password/:id', users.UpdatePassword);

api.get('/verify/user', users.VerifyUser);


module.exports = api;
