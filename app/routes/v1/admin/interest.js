const express = require('express');

const api = express.Router();
const interest = require('../../.././rpcClients/interestCli');

api.put('/:id', interest.UpdateInterest);
api.post('/', interest.CreateInterest);
api.delete('/:', interest.DeleteInterest);

module.exports = api;
