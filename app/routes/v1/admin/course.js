const express = require('express');

const api = express.Router();
const course = require('../../.././rpcClients/course');

api.route('/')
  .post(course.CreateCourse)
  .get(course.GetCourses);

api.route('/edit/:id')
  .get(course.GetCourse)
  .put(course.UpdateCourse)
  .delete(course.DeleteCourse);

api.get('/trending', course.GetTrendingCourse);
api.get('/featured', course.GetFeaturedCourse);
api.get('/compulsory/:organization', course.GetCompulsoryCourses);
api.get('/search', course.SearchCourse);


module.exports = api;
