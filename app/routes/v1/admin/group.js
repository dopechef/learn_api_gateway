const express = require('express');

const api = express.Router();
const groups = require('../../.././rpcClients/groupsCli');

// router.use(auth.validateToken);
api.route('/')
  .post(groups.CreateGroup);


api.route('/:id')
  .get(groups.GetGroup)
  .put(groups.UpdateGroup)
  .delete(groups.DeleteGroup);

api.get('/organization/:id', groups.GetGroups);

api.post('/verify/group', groups.VerifyGroup);


module.exports = api;
