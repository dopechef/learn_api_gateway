const express = require('express');

const api = express.Router();
const organizations = require('../../.././rpcClients/organizationCli');

// router.use(auth.validateToken);
api.route('/')
  .post(organizations.CreateOrganization);

api.get('/name/:name', organizations.GetOrganizationByName);

api.route('/:id')
  .get(organizations.GetOrganization)
  .put(organizations.UpdateOrganization)
  .delete(organizations.DeleteOrganization);

api.get('/:id/members', organizations.GetMembers);


module.exports = api;
