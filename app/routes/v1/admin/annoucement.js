const express = require('express');

const api = express.Router();
const announcement = require('../../.././rpcClients/annoucementCli');


api.get('/:id', announcement.GetAnnouncement);

api.post('/', announcement.CreateAnnouncement);

api.put('/:id', announcement.UpdateAnnouncement);

api.delete('/:id', announcement.DeleteAnnouncement);

module.exports = api;
