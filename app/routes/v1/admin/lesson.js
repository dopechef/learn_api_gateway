const express = require('express');

const api = express.Router();
const lesson = require('../../.././rpcClients/course');

api.route('/')
  .post(lesson.CreateLesson)
  .get(lesson.GetLessons);

api.route('/upload')
  .post(lesson.UploadLesson);

api.route('/edit/:id')
  .get(lesson.GetLesson)
  .put(lesson.UpdateLesson)
  .delete(lesson.DeleteLesson);


module.exports = api;
