const express = require('express');

const api = express.Router();
const groups = require('../../.././rpcClients/groupsCli');


api.route('/:id')
  .get(groups.GetGroup);

api.get('/organization/:id', groups.GetGroups);


module.exports = api;
