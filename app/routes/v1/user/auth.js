const express = require('express');

const app = express();

const api = express.Router();
const users = require('../../.././rpcClients/userCli');
const { validateUserToken } = require('../../../middleware/auth');

// enable authentication
app.use(validateUserToken);
// User signup
api.post('/signup', users.CreateUser);


// User login
api.post('/login', users.Auth);

// Verify user email with the verification code
api.post('/verifycode', users.VerifyUserCode);

api.put('/password/:id', users.UpdatePassword);


module.exports = api;
