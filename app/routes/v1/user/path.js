const express = require('express');

const api = express.Router();
const path = require('../../.././rpcClients/pathCli');

api.route('/review')
  .post(path.reviewPath);

api.route('/editpath/:id')
  .get(path.GetPath);

api.get('/organization/:id', path.GetByOrg);
api.get('/user/:id', path.GetByUser);

api.get('/trending', path.GetTrendingPath);
api.get('/featured', path.GetFeaturedPath);
api.get('/search', path.SearchPath);
api.get('/foryou', path.ForYou);


module.exports = api;
