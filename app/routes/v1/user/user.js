const express = require('express');

const app = express();

const api = express.Router();
const users = require('../../.././rpcClients/userCli');
const { validateUserToken } = require('../../../middleware/auth');

// enable authentication
app.use(validateUserToken);

api.put('/interest/:id', users.AddInterest);

api.route('/:id')
  .put(users.UpdateUser);

api.put('/path/:id', users.AddPaths);


module.exports = api;
