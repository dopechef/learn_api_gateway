const express = require('express');

const api = express.Router();
const announcement = require('../../.././rpcClients/annoucementCli');

api.get('/:id', announcement.GetAnnouncement);

module.exports = api;
