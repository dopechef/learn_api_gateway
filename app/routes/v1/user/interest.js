const express = require('express');

const api = express.Router();
const interest = require('../../.././rpcClients/interestCli');

api.get('/', interest.GetInterests);

module.exports = api;
