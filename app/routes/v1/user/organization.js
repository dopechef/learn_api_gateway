const express = require('express');

const api = express.Router();
const organizations = require('../../.././rpcClients/organizationCli');

api.route('/:id')
  .get(organizations.GetOrganization);

api.get('/:id/members', organizations.GetMembers);


module.exports = api;
