const express = require('express');

const api = express.Router();
const course = require('../../.././rpcClients/course');

api.route('/')
  .get(course.GetCourses);

api.route('/edit/:id')
  .get(course.GetCourse);

api.get('/trending', course.GetTrendingCourse);
api.get('/featured', course.GetFeaturedCourse);
api.get('/compulsory/:user', course.GetCompulsoryCourses);
api.get('/search', course.SearchCourse);


module.exports = api;
