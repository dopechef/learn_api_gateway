const express = require('express');

const api = express.Router();
const lesson = require('../../.././rpcClients/course');

api.route('/')
  .get(lesson.GetLessons);

api.route('/upload')
  .post(lesson.UploadLesson);

api.route('/edit/:id')
  .get(lesson.GetLesson);


module.exports = api;
