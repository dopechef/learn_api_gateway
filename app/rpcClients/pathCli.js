const proto = require('../includes/proto');

const { PathRPC } = proto;

const CreatePath = (req, res) => {
  PathRPC.CreatePath(req.body, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const ForYou = (req, res) => {
  const { id } = req.params;
  PathRPC.ForYou({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};


const GetPath = (req, res) => {
  const { id } = req.params;
  PathRPC.GetPath({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetTrendingPath = (req, res) => {
  PathRPC.SearchPath({ search: JSON.stringify('trending') }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetFeaturedPath = (req, res) => {
  PathRPC.SearchPath({ search: JSON.stringify('featured') }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const SearchPath = (req, res) => {
  const { search } = req.query;
  PathRPC.SearchPath({ search: JSON.stringify(search) }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetByUser = (req, res) => {
  const paths = req.params;
  PathRPC.GetByUser({ id: JSON.stringify(paths) }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetByOrg = (req, res) => {
  const paths = req.params;
  PathRPC.GetByOrg({ id: JSON.stringify(paths) }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};


const UpdatePath = (req, res) => {
  const { id } = req.params;
  PathRPC.UpdatePath({
    id,
    body: JSON.stringify(req.body),

  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const DeletePath = (req, res) => {
  const { id } = req.params;
  PathRPC.DeletePath({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};
const reviewPath = (req, res) => {
  console.log(req.body);
  PathRPC.reviewPath(req.body, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};


module.exports = {
  CreatePath,
  GetPath,
  ForYou,
  GetFeaturedPath,
  GetTrendingPath,
  SearchPath,
  GetByUser,
  GetByOrg,
  UpdatePath,
  DeletePath,
  reviewPath,
};
