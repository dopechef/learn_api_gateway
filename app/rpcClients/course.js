const Joi = require('joi');

const proto = require('../includes/proto');

const { CourseRPC } = proto;
const { LessonRPC } = proto;
const { OrganizeRPC } = proto;
const { singleUpload } = require('../../config/config');

const UploadLesson = async (req, res) => {
  try {
    const contents = req.files;

    console.log(contents);

    const file = await singleUpload(contents);
    LessonRPC.UploadLesson({
      body: JSON.stringify({
        public_id: file.public_id,
        url: file.url,
        secure_url: file.secure_url,
        format: file.format,
        duration: file.duration,
        pages: file.pages,
      }),
    }, (err, response) => {
      if (response) {
        response.data = JSON.parse(response.data);
        return res.json(response);
      }
      console.log(err);
      return res.json({
        success: false,
        data: JSON.parse(err),
      });
    });
  } catch (error) {
    console.log(error);
  }
};


const CreateLesson = async (req, res) => {
  try {
    const schema = Joi.object().keys({
      name: Joi.string().required(),
      contentType: Joi.string().required(),
      completed: Joi.bool().required(),
      length: Joi.string().required(),
      content: Joi.string().required(),
      course_id: Joi.string().required(),
      description: Joi.string().required(),
    });

    const validateResult = Joi.validate(req.body[0], schema);

    if (validateResult.error !== null) {
      return res.json({
        success: false,
        message: validateResult.error.details[0].message,
      });
    }
    // eslint-disable-next-line
    for (req.body of req.body) {
      LessonRPC.AddLessons({
        lessons: JSON.stringify({
          name: req.body.name,
          contentType: req.body.contentType,
          completed: req.body.completed,
          length: req.body.length,
          content: req.body.content,
          course_id: req.body.course_id,
          description: req.body.description,

        }),
      }, (err, response) => {
        if (response) {
          response.data = JSON.parse(response.data);
          return res.json(response);
        }
        console.log(err);
        return res.json({
          success: false,
          data: JSON.parse(err),
        });
      });
    }
  } catch (err) {
    console.log(err);
  }
};


const GetLesson = (req, res) => {
  const { id } = req.params;

  LessonRPC.GetLesson({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetLessons = (req, res) => {
  const { courseId } = req.params;
  const { contentType } = req.query;

  LessonRPC.GetLessons({ courseId, contentType }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const UpdateLesson = (req, res) => {
  LessonRPC.UpdateLesson({ id: req.params.id, body: JSON.stringify(req.body) }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const DeleteLesson = (req, res) => {
  const { id } = req.params;

  LessonRPC.DeleteLesson({ id }, (err, response) => {
    if (response) {
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};


const CreateCourse = (req, res) => {
  const schema = Joi.object().keys({
    name: Joi.string().required(),
    description: Joi.string().required(),
    image: Joi.string().required(),
    author: Joi.string().required(),
    status: Joi.string().required(),
    public: Joi.bool().required(),
    completed: Joi.bool().required(),
    length: Joi.string().required(),
    published_to: Joi.string().required(),
    category: Joi.string().required(),
  });

  const validateResult = Joi.validate(req.body, schema);

  if (validateResult.error !== null) {
    return res.json({
      success: false,
      message: validateResult.error.details[0].message,
    });
  }

  CourseRPC.AddCourses(req.body, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetCourse = (req, res) => {
  const { id } = req.params;

  CourseRPC.GetCourse({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetTrendingCourse = (req, res) => {
  CourseRPC.SearchCourse({ search: JSON.stringify('trending') }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetFeaturedCourse = (req, res) => {
  CourseRPC.SearchCourse({ search: JSON.stringify('featured') }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetCompulsoryCourses = (req, res) => {
  // get the organization id by its name
  const { user } = req.params;

  CourseRPC.GetCompulsoryCourses({ user }, (err, response) => {
    console.log(response);
    if (response) {
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const SearchCourse = (req, res) => {
  const { search } = req.query;
  CourseRPC.SearchCourse({ search: JSON.stringify(search) }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetCourses = (req, res) => {
  const { category } = req.query;

  CourseRPC.GetCourses({ category }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const UpdateCourse = (req, res) => {
  CourseRPC.UpdateCourse({ id: req.params.id, body: JSON.stringify(req.body) }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const DeleteCourse = (req, res) => {
  const { id } = req.params;

  CourseRPC.DeleteCourse({ id }, (err, response) => {
    if (response) {
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};


module.exports = {
  CreateLesson,
  GetLesson,
  GetLessons,
  UpdateLesson,
  DeleteLesson,
  CreateCourse,
  GetCourse,
  GetTrendingCourse,
  GetFeaturedCourse,
  GetCompulsoryCourses,
  SearchCourse,
  GetCourses,
  UpdateCourse,
  DeleteCourse,
  UploadLesson,
};
