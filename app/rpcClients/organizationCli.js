const proto = require('../includes/proto');

const { OrganizeRPC } = proto;

const CreateOrganization = (req, res) => {
  OrganizeRPC.CreateOrganization(req.body, (err, response) => {
    if (response) {
      console.log(response);
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetOrganization = (req, res) => {
  const { id } = req.params;
  OrganizeRPC.GetOrganization({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetOrganizationByName = (req, res) => {
  const { name } = req.params;
  OrganizeRPC.GetOrganizationByName({ name }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetMembers = (req, res) => {
  const { id } = req.params;
  OrganizeRPC.GetMembers({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const UpdateOrganization = (req, res) => {
  const { id } = req.params;
  const {
    name, email, mobile, avatar, suspended,
  } = req.body;
  OrganizeRPC.UpdateOrganization({
    id,
    body: JSON.stringify(req.body),
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const DeleteOrganization = (req, res) => {
  const { id } = req.params;
  OrganizeRPC.DeleteOrganization({ id }, (err, response) => {
    if (response) {
      // response.data = JSON.parse(response.data)
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const RestoreOrganization = (req, res) => {
  const { id } = req.params;
  OrganizeRPC.RestoreOrganization({ id }, (err, response) => {
    if (response) {
      // response.data = JSON.parse(response.data)
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};


module.exports = {
  CreateOrganization,
  GetOrganization,
  GetOrganizationByName,
  GetMembers,
  UpdateOrganization,
  DeleteOrganization,
  RestoreOrganization,
};
