const proto = require('../includes/proto');

const { InterestRPC } = proto;

const CreateInterest = (req, res) => {
  InterestRPC.CreateInterest(req.body, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetInterests = (req, res) => {
  InterestRPC.GetInterests({}, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};


const UpdateInterest = (req, res) => {
  InterestRPC.UpdateInterest({ id: req.params.id, body: JSON.stringify(req.body) },
    (err, response) => {
      if (response) {
        response.data = JSON.parse(response.data);
        return res.json(response);
      }
      return res.json({
        success: false,
        data: err,
      });
    });
};

const DeleteInterest = (req, res) => {
  const { id } = req.params;
  InterestRPC.DeleteInterest({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const RestoreInterest = (req, res) => {
  const { id } = req.params;
  InterestRPC.RestoreInterest({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};


module.exports = {
  CreateInterest,
  GetInterests,
  UpdateInterest,
  DeleteInterest,
  RestoreInterest,
};
