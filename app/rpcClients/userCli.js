const Joi = require('joi');
const jwt = require('jsonwebtoken');
const proto = require('../includes/proto');

const { UserRPC } = proto;

const CreateUser = async (req, res) => {
  console.log(req.body);
  const {
    firstname, lastname, organization, email, role,
  } = req.body;
  console.log(firstname, lastname, organization, email, role);
  try {
    const schema = Joi.object()
      .keys({
        firstname: Joi.string().required(),
        lastname: Joi.string().required(),
        organization: Joi.string().required(),
        email: Joi.string()
          .email()
          .required(),
        role: Joi.string().required(),
      });

    const validation = Joi.validate(req.body, schema);
    if (validation.error !== null) throw new Error(validation.error.details[0].message);

    await UserRPC.CreateUser(req.body, (err, response) => {
      if (response) {
        response.data = JSON.parse(response.data);
        // Generate token
        const token = jwt.sign({ id: response.data._id }, process.env.JWT_SECRET, { expiresIn: '1d' });
        return res.json({ data: response.data, token });
      }

      console.log(err);
      return res.json({
        success: false,
        data: err,
      });
    });
  } catch (err) {
    console.error(err);
    return res.json({
      success: false,
      data: err,
    });
  }
};

const Auth = async (req, res) => {
  const { email, password } = req.body;
  const schema = Joi.object()
    .keys({
      email: Joi.string()
        .email()
        .optional(),
      password: Joi.string().required(),
    });
  const validation = Joi.validate(req.body, schema);
  if (validation.error !== null) {
    res.status(400).send({
      success: false,
      message: validation.error.details[0].message,
    });
    return;
  }
  console.log('logging');

  try {
    const resp = await UserRPC.authenticateUserWithEmailAndPassword({
      email,
      password,
    });

    console.log(resp);
    if (!resp.data) {
      res.json({ success: false, data: null, message: resp.message });
      return;
    }

    const user = JSON.parse(resp.data);
    // Generate token
    const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, { expiresIn: '1d' });
    // Update user last login...
    await UserRPC.UpdateUser({ id: user._id, body: JSON.stringify({ last_logedin: new Date() }) });
    console.log(user.message);
    if (!user.message) {
      res.json({ success: true, data: user, token });
      return;
    }
    res.json({ success: false, data: {}, message: user.message });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, error });
  }
};

const GetUser = (req, res) => {
  const {
    id,
  } = req.params;
  UserRPC.GetUser({
    id,
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetAll = (req, res) => {
  const {
    organization,
  } = req.query;
  UserRPC.GetUsers({
    organization,
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};


const GetByGroup = (req, res) => {
  const {
    organization,
    group,
  } = req.query;
  UserRPC.GetByGroup({
    organization,
    group,
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};


const GetByRole = (req, res) => {
  const {
    organization,
    role,
  } = req.query;
  UserRPC.GetByRole({
    organization,
    role,
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};


const UpdatePassword = (req, res) => {
  const {
    id,
  } = req.params;
  const {
    password,
  } = req.body;
  UserRPC.UpdatePassword({
    id,
    password,
  }, (err, response) => {
    if (response) {
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const UpdateUser = (req, res) => {
  const {
    id,
  } = req.params;
  UserRPC.UpdateUser({
    id,
    body: JSON.stringify(req.body),
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const DeleteUser = (req, res) => {
  const {
    id,
  } = req.params;
  UserRPC.DeleteUser({
    id,
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const RestoreUser = (req, res) => {
  const {
    id,
  } = req.params;
  UserRPC.RestoreUser({
    id,
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const AddInterest = (req, res) => {
  const {
    id,
  } = req.params;
  const {
    interest,
  } = req.body;
  UserRPC.AddInterest({
    id,
    interest: JSON.stringify(interest),
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const AddPaths = (req, res) => {
  const {
    id,
  } = req.params;
  const {
    paths,
  } = req.body;

  UserRPC.AddPaths({
    id,
    paths: JSON.stringify(paths),

  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const GetUsers = (req, res) => {
  const {
    query,
  } = req.query;
  switch (query) {
    case 'all':
      GetAll(req, res);
      break;
    case 'role':
      GetByRole(req, res);
      break;
    case 'group':
      GetByGroup(req, res);
      break;
    default:
      res.json({
        success: false,
        message: 'Invalid params',
      });
      break;
  }
};

const GetDeletedUsers = (req, res) => {
  const {
    organization,
  } = req.query;
  UserRPC.GetDeletedUsers({
    organization,
  }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const VerifyUser = (req, res) => {
  const {
    email,
  } = req.query;
  UserRPC.VerifyUser({
    email,
  }, (err, response) => {
    if (response) {
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

const VerifyUserCode = (req, res) => {
  const {
    email, verificationcode,
  } = req.body;
  UserRPC.verifyUserVerificationCode({
    email, verificationcode,
  }, (err, response) => {
    if (response) {
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

module.exports = {
  CreateUser,
  Auth,
  GetUser,
  GetUsers,
  GetDeletedUsers,
  GetByGroup,
  GetByRole,
  UpdatePassword,
  UpdateUser,
  DeleteUser,
  RestoreUser,
  AddInterest,
  AddPaths,
  VerifyUser,
  VerifyUserCode,
};
