const proto = require('../includes/proto');

const { GroupRPC } = proto;

const CreateGroup = (req, res) => {
  GroupRPC.CreateGroup(req.body, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetGroup = (req, res) => {
  const { id } = req.params;
  GroupRPC.GetGroup({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};


const GetGroups = (req, res) => {
  const { id } = req.params;
  GroupRPC.GetGroups({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const UpdateGroup = (req, res) => {
  const { id } = req.params;
  GroupRPC.UpdateGroup({ id, body: JSON.stringify(req.body) }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const DeleteGroup = (req, res) => {
  const { id } = req.params;
  GroupRPC.DeleteGroup({ id }, (err, response) => {
    if (response) {
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const VerifyGroup = async (req, res) => {
  const { name, organization } = req.body;

  console.log(name);

  GroupRPC.VerifyGroup({
    name,
    organization,
  }, (err, response) => {
    if (response) {
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: err,
    });
  });
};

module.exports = {
  CreateGroup,
  GetGroup,
  GetGroups,
  UpdateGroup,
  DeleteGroup,
  VerifyGroup,
};
