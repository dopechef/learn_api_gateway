const proto = require('../includes/proto');

const { AnnouncementRPC } = proto;

const CreateAnnouncement = (req, res) => {
  AnnouncementRPC.CreateAnnouncement(req.body, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetAnnouncement = (req, res) => {
  const { id } = req.params;

  AnnouncementRPC.GetAnnouncement({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const GetAnnouncementsByUser = (req, res) => {
  const paths = req.params;
  AnnouncementRPC.GetAnnouncementsByUser({ user_id: JSON.stringify(paths) }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    return res.json({
      success: false,
      data: err,
    });
  });
};

const UpdateAnnouncement = (req, res) => {
  AnnouncementRPC.UpdateAnnouncement({ id: req.params.id, body: JSON.stringify(req.body) },
    (err, response) => {
      if (response) {
        response.data = JSON.parse(response.data);
        return res.json(response);
      }
      return res.json({
        success: false,
        data: err,
      });
    });
};

const DeleteAnnouncement = (req, res) => {
  const { id } = req.params;
  AnnouncementRPC.DeleteAnnouncement({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};

const RestoreAnnouncement = (req, res) => {
  const { id } = req.params;
  AnnouncementRPC.RestoreAnnouncement({ id }, (err, response) => {
    if (response) {
      response.data = JSON.parse(response.data);
      return res.json(response);
    }
    console.log(err);
    return res.json({
      success: false,
      data: JSON.parse(err),
    });
  });
};


module.exports = {
  CreateAnnouncement,
  GetAnnouncement,
  GetAnnouncementsByUser,
  UpdateAnnouncement,
  DeleteAnnouncement,
  RestoreAnnouncement,
};
