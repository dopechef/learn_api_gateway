const jwt = require('jsonwebtoken');
const proto = require('../includes/proto');

const { UserRPC } = proto;

function validateUserToken(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }

  const token = req.headers.authorization.substr(7);
  jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
    if (err) {
      res.status(403).send({
        success: false,
        message: 'Failed to authenticate token.',
        error: err,
      });
      return;
    }

    try {
      const data = await UserRPC.GetUser({ id: decoded.id });
      if (!data.data) {
        res.status(403).send({
          success: false,
          message: 'Invalid token.',
        });
        return;
      }

      req.token = decoded;
      req.user = JSON.parse(data.data);

      next();
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, error });
    }
  });

  return null;
}

module.exports = {
  validateUserToken,
};
