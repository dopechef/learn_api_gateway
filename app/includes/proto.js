const caller = require('grpc-caller');
require('dotenv').config();

const UserRPC = caller(
  `${process.env.USER_SERVICE_IP}:${process.env.USER_SERVICE_PORT}`,
  `${process.env.PROTO_LOCATION}/user.proto`,
  'User',
);

const CourseRPC = caller(
  `${process.env.COURSE_SERVICE_IP}:${process.env.COURSE_SERVICE_PORT}`,
  `${process.env.PROTO_LOCATION}/course.proto`,
  'Courses',
);

const LessonRPC = caller(
  `${process.env.COURSE_SERVICE_IP}:${process.env.COURSE_SERVICE_PORT}`,
  `${process.env.PROTO_LOCATION}/course.proto`,
  'Lessons',
);

const PathRPC = caller(
  `${process.env.PATH_SERVICE_IP}:${process.env.PATH_SERVICE_PORT}`,
  `${process.env.PROTO_LOCATION}/path.proto`,
  'Path',
);

const OrganizeRPC = caller(
  `${process.env.USER_SERVICE_IP}:${process.env.USER_SERVICE_PORT}`,
  `${process.env.PROTO_LOCATION}/organization.proto`,
  'Org',
);

const GroupRPC = caller(
  `${process.env.USER_SERVICE_IP}:${process.env.USER_SERVICE_PORT}`,
  `${process.env.PROTO_LOCATION}/groups.proto`,
  'Group',
);

const AnnouncementRPC = caller(
  `${process.env.PATH_SERVICE_IP}:${process.env.PATH_SERVICE_PORT}`,
  `${process.env.PROTO_LOCATION}/announcement.proto`,
  'Announcement',
);

const InterestRPC = caller(
  `${process.env.PATH_SERVICE_IP}:${process.env.PATH_SERVICE_PORT}`,
  `${process.env.PROTO_LOCATION}/interest.proto`,
  'Interest',
);

module.exports = {
  CourseRPC,
  LessonRPC,
  UserRPC,
  PathRPC,
  OrganizeRPC,
  GroupRPC,
  AnnouncementRPC,
  InterestRPC,
};
