const amqp = require('amqplib/callback_api');
const url = require('url');

require('dotenv').config();

const parsedURI = url.parse(process.env.AMQP_HOST1);
// console.log(parsedURI);


module.exports = {
  // attachments = array of object attachments. more info: https://nodemailer.com/using-attachments/
  send(data) {
    this.sendSMSUsingProvidus(data);
  },
  sendSMSUsingProvidus(data) {
    amqp.connect(process.env.AMQP_HOST1, { servername: parsedURI.hostname }, (err, conn) => {
      conn.createChannel((errr, ch) => {
        if (errr) {
          console.error(err);
          return;
        }
        const q = 'sms_eyowo';
        const msg = JSON.stringify(data);

        console.log(msg);

        ch.assertQueue(q, { durable: true });
        ch.sendToQueue(q, Buffer.from(msg), { persistent: true });
        console.log(" [x] Sent '%s'", msg);
      });
    });
  },
  sendSMSUsingEbulkSMS(data) {
    amqp.connect(process.env.AMQP_HOST1, { servername: parsedURI.hostname }, (err, conn) => {
      conn.createChannel((errr, ch) => {
        if (errr) {
          console.error(err);
          return;
        }
        const q = 'sms';
        const msg = JSON.stringify(data);

        console.log(msg);

        ch.assertQueue(q, { durable: true });
        ch.sendToQueue(q, Buffer.from(msg), { persistent: true });
        console.log(" [x] Sent '%s'", msg);
      });
    });
  },
  sendUsingBOIGateway(data) {
    amqp.connect(process.env.AMQP_HOST1, { servername: parsedURI.hostname }, (err, conn) => {
      conn.createChannel((errr, ch) => {
        if (errr) {
          console.error(err);
          return;
        }
        const q = 'sms_boi';
        const msg = JSON.stringify(data);

        console.log(msg);

        ch.assertQueue(q, { durable: true });
        ch.sendToQueue(q, Buffer.from(msg), { persistent: true });
        console.log(" [x] Sent '%s'", msg);
      });
    });
  },
};
