const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const { env, jwtSecret, jwtExpirationInterval } = require('../config/config');

const { Schema } = mongoose;

const UserSchema = new Schema({
  name: {
    type: String,
    maxlength: 128,
    index: true,
    trim: true,
  },
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
  },
  avater: {
    type: String,
    trim: true,
  },
  group: {
    type: Array, // [mongoose.Types.ObjectId]
  },
  interest: {
    type: Array,
  },
  role: {
    type: String,
  },
  mobile: {
    type: String,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 128,
  },
  suspended: {
    type: Boolean,
  },
  verified: {
    type: Boolean,
  },
  firstname: {
    type: String,
  },
  lastname: {
    type: String,
  },
  organization: {
    type: String,
  },
  paths: {
    type: Array,
  },
  last_logedin: {
    type: Date,
  },
}, {
  timestamps: true,
});

async function SchemaPreSave(next) {
  const user = this;

  if (this.isNew && this.isActive == null) {
    // status is for activate and deactivate
    this.status = true;
    this.isDeleted = false;
  }
  if (this.isModified('password') || this.isNew) {
    try {
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(user.password, salt);
      user.password = hash;
      next();
    } catch (e) {
      next(e);
    }
  } else {
    return next();
  }
}

UserSchema.pre('save', SchemaPreSave);


async function passConfirm(passw) {
  try {
    const res = await bcrypt.compare(passw, this.password);
    return Promise.resolve(res);
  } catch (e) {
    return Promise.reject(e);
  }
}

UserSchema.methods.comparePassword = passConfirm;


module.exports = mongoose.model('User', UserSchema);
